#!/bin/bash

USE_MULTIMEDIA=${USE_MULTIMEDIA:-0}
USE_BASEIMAGE=${USE_BASEIMAGE:-0}
IMAGE_NAME="ubuntu.ext4"

_HAS_BASEIMAGE_=0
if [ -f "${IMAGE_NAME}.base" ]; then
	_HAS_BASEIMAGE_=1
fi

set -e

if [ "$EUID" -ne 0 ]; then
	>&2 echo [-] Root privilege is required to build the Ubuntu image
	exit
fi

# install pre-required host packages
_CHECK_=`apt list --installed 2>/dev/null | grep -E "^(debootstrap|qemu-user-static|binfmt-support)\/" | wc -l`
if [ $_CHECK_ -ne 3 ]; then
	echo [!] Install required packages
	apt-get update
	apt-get install -y \
		debootstrap \
		qemu-user-static \
		binfmt-support

	update-binfmts --import qemu-aarch64
fi
unset _CHECK_

# prepare image and directories
echo [!] Prepare workspace
SRCDIR="src"
ROOTFS="rootfs"

function mount_all
{
	if [ -d "${ROOTFS}" ]; then
		return
	fi

	mkdir -p "${ROOTFS}"
	mount "${IMAGE_NAME}" "${ROOTFS}"

	mkdir -p "${ROOTFS}/src"
	mount --bind -o ro "${SRCDIR}" "${ROOTFS}/src"

	mkdir -p "${ROOTFS}/tmp"
	mount -t tmpfs tmpfs "${ROOTFS}/tmp"
}

function unmount_all {
	if ! [ -d "${ROOTFS}" ]; then
		return
	fi

	umount "${ROOTFS}/src"
	rmdir "${ROOTFS}/src"

	umount "${ROOTFS}/tmp"

	umount "${ROOTFS}"
	rmdir "${ROOTFS}"
}

trap unmount_all EXIT INT TERM

# run setup scripts
function run() {
	echo [!] Run Scripts: $1
	SCRIPT_NAME=$1
	chroot "${ROOTFS}" /bin/sh -- "/src/${SCRIPT_NAME}"
}

# prepare base image
function prepare_baseimage {
	dd if=/dev/zero of="${IMAGE_NAME}" bs=1024 count=1 status=none
	dd if=/dev/zero of="${IMAGE_NAME}" bs=1024 count=0 seek=5767168 status=none
	mkfs.ext4 -q -b 4096 "${IMAGE_NAME}"

	mount_all

	# initial debian bootstraping
	echo [!] Initial debian bootstraping
	debootstrap \
		--arch arm64 \
		--foreign jammy \
		"${ROOTFS}" \
		http://ports.ubuntu.com/ubuntu-ports/

	chroot "${ROOTFS}" /debootstrap/debootstrap --second-stage

	# basic initialization
	run init.sh

	unmount_all
}

if [ $USE_BASEIMAGE -eq 1 ]; then
	if [ $_HAS_BASEIMAGE_ -eq 0 ]; then
		prepare_baseimage
		rm -f "${IMAGE_NAME}.base"
		cp --sparse=always "${IMAGE_NAME}" "${IMAGE_NAME}.base"
	fi

	rm -f "${IMAGE_NAME}"
	cp --sparse=always "${IMAGE_NAME}.base" "${IMAGE_NAME}"
else
	prepare_baseimage
fi

mount_all

# Tweaks
run tweaks.sh

# GUI
run gui.sh

# Networks
run networks.sh

# Prebuilts
run prebuilts.sh

# Multimedia
if [ $USE_MULTIMEDIA -eq 1 ]; then
	run multimedia.sh
fi

# Clean-up
run cleanup.sh
