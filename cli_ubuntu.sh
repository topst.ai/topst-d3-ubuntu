#!/bin/bash

set -e

if [ "$EUID" -ne 0 ]; then
	>&2 echo [-] Root privilege is required to build the Ubuntu image
	exit
fi

SRCDIR="src"
ROOTFS="rootfs"
IMAGE_NAME="ubuntu.ext4"

function unmount_all {
	if ! [ -d "${ROOTFS}" ]; then
		return
	fi

	umount "${ROOTFS}/src"
	rmdir "${ROOTFS}/src"

	umount "${ROOTFS}/tmp"

	umount "${ROOTFS}"
	rmdir "${ROOTFS}"
}

trap unmount_all EXIT INT TERM

if ! [ -d "${ROOTFS}" ]; then
	mkdir -p "${ROOTFS}"
	mount "${IMAGE_NAME}" "${ROOTFS}"

	mkdir -p "${ROOTFS}/src"
	mount --bind -o ro "${SRCDIR}" "${ROOTFS}/src"

	mkdir -p "${ROOTFS}/tmp"
	mount -t tmpfs tmpfs "${ROOTFS}/tmp"
fi

chroot "${ROOTFS}" /bin/bash
