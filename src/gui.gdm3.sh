#!/bin/bash

# install GUI components
DEBIAN_FRONTEND=noninteractive apt-get install -y \
	gdm3 \
	xubuntu-desktop \
	xwayland \
	eog

# Enalbe Wayland
sed -i 's/#WaylandEnable=false/WaylandEnable=true/' /etc/gdm3/custom.conf
