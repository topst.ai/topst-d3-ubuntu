#!/bin/sh

# set locale
locale-gen --purge en_US.UTF-8
echo 'LANG="en_US.UTF-8"\nLANGUAGE="en_US:en"\n' > /etc/default/locale

# basic components
apt-get install -y \
	bash \
	bash-completion \
	git \
	htop \
	minicom \
	p7zip-full

# force install latest linux-firmware
apt-get install -y --reinstall linux-firmware

# set root password (PW: root)
echo 'root:root' | chpasswd

# add user topst (PW: topst)
useradd -m -G sudo -s /bin/bash topst
echo 'topst:topst' | chpasswd

adduser topst dialout
adduser topst disk
adduser topst kmem
adduser topst tty

# mount points
cp /src/fstab /etc/fstab
