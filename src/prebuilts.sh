#!/bin/sh

for DEB in $(find /src/prebuilts -type f -name *.deb)
do
	PACKAGE_NAME=`dpkg -I $DEB | grep Package: | awk -F': ' '{print $2}'`

	if dpkg -s $PACKAGE_NAME 2>/dev/null 1>/dev/null; then
		echo " * Package: $PACKAGE_NAME is already installed"
	else
		dpkg -i --force-depends --force-bad-version --no-triggers $DEB
	fi
done

cp -r /src/prebuilts/modules-load.d/* /etc/modules-load.d/

# hacks
sed -i 's/^Depends: kernel-5.4/#Depends: kernel-5.4/g' /var/lib/dpkg/status
sed -i 's/libdrm /libdrm2 /g' /var/lib/dpkg/status
sed -i 's/libgcc1 (>= arm-9.2), libstdc++6 (>= arm-9.2), //g' /var/lib/dpkg/status

# fixup
apt --fix-broken install -y
