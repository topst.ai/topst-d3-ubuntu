#!/bin/sh

echo 'nameserver 8.8.8.8' > /etc/resolv.conf
echo 'nameserver 8.8.4.4' >> /etc/resolv.conf
echo 'TOPST' > /etc/hostname
echo '127.0.1.1		TOPST' >> /etc/hosts

# network tools
apt-get install -y \
	curl \
	iw \
	netcat \
	net-tools \
	ssh \
	wpasupplicant

# configure Netplan
cat > /etc/netplan/99-default.yaml << EOF
network:
  version: 2
  ethernets:
    eth0:
      dhcp4: true
      optional: true
EOF
