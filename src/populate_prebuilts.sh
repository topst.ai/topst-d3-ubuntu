#!/bin/bash

set -e

if [ "$#" -ne 1 ]; then
	cat << EOL
[!] Usage: ${0} buildroot_dir

Required:
  buildroot_dir     build-main directory
EOL
fi

BUILDROOT=$(realpath $1)

if ! [ -d "${BUILDROOT}/conf" ]; then
	>&2 echo [-] Invalid build root directory: $BUILDROOT
	exit
fi

if ! [ -d "${BUILDROOT}/tmp/deploy/deb" ]; then
	>&2 echo [-] No deb packages
	>&2 echo "    Please add package_deb in PACKAGE_CLASSES"
	exit
fi

if ! [ -f "${BUILDROOT}/tmp/deploy/images/tcc8050-main/topst-tcc8050-main.ext4" ]; then
	>&2 echo [-] Build yocto image first
	exit
fi

DEBROOT="${BUILDROOT}/tmp/deploy/deb"
echo [!] Target directory: $DEBROOT

cd "$(dirname "$0")"

rm -rf prebuilts
mkdir -p prebuilts

echo [!] Extract modules-load.d
7z x -oprebuilts -- "${BUILDROOT}/tmp/deploy/images/tcc8050-main/topst-tcc8050-main.ext4" etc/modules-load.d/ >/dev/null

echo [!] Copy Packages
cd prebuilts

mv etc/modules-load.d .
rmdir etc

# vpu tweak
if [ -f modules-load.d/vpu.conf ]; then
	mv modules-load.d/vpu.conf modules-load.d/01-vpu.conf
	mv modules-load.d/vpu-lib.conf modules-load.d/00-vpu-lib.conf
fi

# Gfx & Media Packages
mkdir aarch64
find "${DEBROOT}/aarch64" -type f -name '*.deb' \
	| grep -E '(pvr|omx_|telechips_|libcdk-(audio|video|demux)_)' \
	| xargs -I{} cp {} aarch64

find aarch64 -type f -name '*gles2*' -exec rm {} \;

# Kernel Drivers
mkdir tcc8050_main
find "${DEBROOT}/tcc8050_main" -type f -name '*.deb' \
	| grep -E 'kernel-module' \
	| xargs -I{} cp {} tcc8050_main

echo [+] Done
