#!/bin/bash

# install components
apt-get install -y \
	build-essential \
	cmake \
	ffmpeg \
	gstreamer1.0-alsa \
	gstreamer1.0-gl \
	gstreamer1.0-plugins-bad \
	gstreamer1.0-plugins-base \
	gstreamer1.0-plugins-base-apps \
	gstreamer1.0-plugins-good \
	gstreamer1.0-plugins-ugly \
	gstreamer1.0-pulseaudio \
	gstreamer1.0-qt5 \
	gstreamer1.0-tools \
	gstreamer1.0-x \
	libavcodec-dev \
	libavformat-dev \
	libgstreamer1.0-0 \
	libgstreamer1.0-dev \
	libgstreamer-plugins-base1.0-dev \
	libgtk2.0-dev \
	libgtk-3-dev \
	libjpeg-dev \
	libopencv-dev \
	libopenexr-dev \
	libpng-dev \
	libswscale-dev \
	libtiff-dev \
	libwebp-dev \
	python3 \
	python3-dev \
	python3-numpy \
	python3-opencv \
	python3-pip

python3 -m pip install -U pip

pip install -U \
	opencv-python \
	opencv-contrib-python
